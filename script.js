// Теоретичне питання
// Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript
// асинхронная функция выполнится в фоновом режиме и покажет результат не сразу при вызове, а спустя время после выполнения остального кода
// Завдання
// Написати програму "Я тебе знайду по IP"

// Технічні вимоги:
// Створити просту HTML-сторінку з кнопкою Знайти по IP.
// Натиснувши кнопку - надіслати AJAX запит за адресою https://api.ipify.org/?format=json, отримати звідти IP адресу клієнта.
// Дізнавшись IP адресу, надіслати запит на сервіс https://ip-api.com/ та отримати інформацію про фізичну адресу.
// під кнопкою вивести на сторінку інформацію, отриману з останнього запиту – континент, країна, регіон, місто, район.
// Усі запити на сервер необхідно виконати за допомогою async await.

async function findByIp() {
  let responseIp = await fetch("https://api.ipify.org/?format=json", {
    method: "GET",
  });
  let userIp = await responseIp.json();
  console.log("User", userIp);

  let responseUser = await fetch(`http://ip-api.com/json/${userIp.ip}`, {
    method: "GET",
  });
  console.log("ResponseUser", responseUser);
  let userInfo = await responseUser.json();
  console.log("userInfo", userInfo);

  const btn = document.getElementById("btn-ip");
  btn.addEventListener("click", () => {
    let infoElem = document.createElement("ul");

    infoElem.innerHTML += `
      <li>Continent:${userInfo.timezone}</li>
      <li>Country:${userInfo.country}</li>
      <li>Region:${userInfo.region}</li>
      <li>City:${userInfo.city}</li>
      <li>Region Name:${userInfo.regionName}</li>
     `;

    document.body.append(infoElem);
  });

  return userInfo;
}

findByIp().catch((error) => console.log("Error", error));
